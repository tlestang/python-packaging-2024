* Notes
=pip= is aimed at packages consumers, [[https://github.com/pypa/pip/issues/6041#issuecomment-516435134][not producers]].

pip wheel exists because pip needs to be able to build a wheel from an
sdist anyway.
* References
- [[https://github.com/pypa/pip/issues/6041][GitHub issue on adding a =pip build= command]]
- [[https://pip.pypa.io/en/stable/cli/pip_wheel/#differences-to-build][=pip wheel= differences to =build=]]
- [[https://peps.python.org/pep-0517/#appendix-a-comparison-to-pep-516][Example of a simple backend implementation]]
- [[https://blog.ganssle.io/articles/2021/10/setup-py-deprecated.html][Why you shouldn't invoke =setup.py= directly]]
- [[https://docs.python.org/3.10/distutils/setupscript.html][Writing the distutils setup script]]
- [[https://packaging.python.org/en/latest/discussions/wheel-vs-egg/][Wheel vs Eggs]]
- [[https://peps.python.org/pep-0632/][PEP 632 - Deprecate distutils]]
- [[https://carpentries-incubator.github.io/python_packaging/instructor/04-history-of-packaging.html][Extra: a description of python build tools]]
- [[https://packaging.python.org/en/latest/discussions/pip-vs-easy-install/][pip vs easy_install]]
